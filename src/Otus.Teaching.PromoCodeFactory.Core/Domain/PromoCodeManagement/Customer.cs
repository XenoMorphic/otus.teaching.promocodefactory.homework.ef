﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        [MaxLength(50)]
        public string Email { get; set; }
        public string Address { get; set; }

        //TODO: Списки Preferences и Promocodes 
        public virtual ICollection<CustomerPreference> PromoCodePreferences { get; set; }

        public virtual ICollection<PromoCode> Promocodes { get; set; }

    }
}